# Introduction

EtherLab is a real time Linux target for Simulink Coder and works best in that
environment.

However, EtherLab supplies a wrapper around `packNGo`. With this, you can
create your model in your favourite environment, generate the code, bundle
everything into a zip file and deploy it on your Linux real time system.

The setup is fairly straight forward:
   * Download [EtherLab](https://gitlab.com/etherlab.org/etherlab/-/releases/permalink/latest/downloads/zipfile)
   * Unpack the zip file somewhere (`$INSTALLDIR`)
   * Start Matlab
   * Make sure a C/C++ compiler is installed
       * `HOME -> Add-Ons -> Get Add-Ons`
       * Search for MinGW-w64
       * Install
   * In Matlab, navigate to `$INSTALLDIR\etherlab-master\rtw`
   * In the command prompt, call `setup_etherlab`. This will take a while. It
     prepares EtherLab and sets up Matlab's search paths. If you've previously
     installed EtherLab, make sure the old search paths are removed
     (`pathtool`)
   * Go to a folder where you want to create your project
   * Open EtherLab library by calling `etherlab_lib` on the command prompt
   * Create a new model
   * Navigate to Model Properties (`MODELING -> Model Settings -> Model Settings`)
     or simply press `Ctrl-E` from your simulink model
       * Then `Code Generation -> System target file -> Browse` and choose `etherlab.tlc`
         or simply type it in to the field (instead of `grt.tlc`).
         Under `Build process`, make sure `Generate code only` is checked
       * In `Solver -> Solver details` choose base sample rate (e.g. 0.001)
   * Add blocks to your model, making sure at least one source block has the
     fastest sample time that your model should run at
   * Save the model
   * Start the code generation process (`Ctrl-B`) hit the `Generate Code` button
   * On the Matlab command promt, type `etherlab_packngo` to generate a zip file
   * Copy the zip file to a Linux real time environment, where `pdserv` and
     possibly `ethercat` is installed
   * unzip the package, dive into the subdirectory
   * start the build process by running `sh build.sh`. Now your executable
     is ready for running.
