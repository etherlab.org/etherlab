EtherLab is a real time Linux target for Simulink Coder and works best in that
environment. You can build your project and deploy them on the same machine.

However, EtherLab supplies a wrapper around Simulink's `packNGo`. With this,
you can create your model in your favourite (non-realtime) environment (Linux
or windows), generate the code, bundle everything into a zip file and deploy it
on your Linux real time target system.

To do this, simply call `etherlab_packngo('<model>')` from the Matlab command
line after generating the code. Make sure `Generate code only` is checked under
`Configuration Parameters -> Build Process -> Generate code only` to prevent
the compliation process from starting.

Copy the zip file to your realtime Linux target, where `pdserv-devel` and
possibly, if required, ethercat-devel` is installed.

Unpack the zip file, navigate into the subdirectory and execute `sh build.sh`
to start the build process. Et Voilà
