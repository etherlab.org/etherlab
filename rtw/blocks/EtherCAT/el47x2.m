%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Encapsulation for oversampling analog output slave EL47x2
%
% Copyright (C) 2013 Richard Hacker
% License: GPLv3+
%
classdef el47x2 < EtherCATSlave

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods
    %====================================================================
    function obj = el47x2(id)
        if nargin > 0
            obj.slave = obj.find(id);
        end
    end

    %========================================================================
    function rv = configure(obj,one_ch,dc_spec,scaling,stno_port)

        % General information
        rv.SlaveConfig.vendor = 2;
        rv.SlaveConfig.product = obj.slave{2};
        rv.SlaveConfig.description = obj.slave{1};

        % Distributed clock
        if dc_spec(1) ~= 15
            % DC Configuration from the default list
            dc = el47x2.dc;
            rv.SlaveConfig.dc = dc(dc_spec(1),:);
        else
            % Custom DC
            rv.SlaveConfig.dc = dc_spec(2:end);
        end

        os_fac = -rv.SlaveConfig.dc(3);
        if os_fac <= 0
            os_fac = 1;
        end

        if one_ch
            channels = 1;
        else
            channels = 1:2;
        end

        % input syncmanager
        for i = channels
            rv.SlaveConfig.sm{i} = {i-1,0,el47x2.pdos{i}};
            entries = rv.SlaveConfig.sm{i}{3}{2}{2};
            rv.SlaveConfig.sm{i}{3}{2}{2} = horzcat(...
                    entries(1)+el47x2.os_idx_inc*(0:os_fac-1)', ...
                    repmat(entries(2:end),os_fac,1));
        end

        fs = [];

        gain = repmat({[]},size(channels));
        if isfield(scaling,'gain') && ~isempty(scaling.gain)
            fs = obj.slave{4};
            gain = arrayfun(@(x) {{strcat('Gain',num2str(x)),
                                  scaling.gain(min(end,x))}}, ...
                            channels);
        end

        offset = repmat({[]},size(channels));
        if isfield(scaling,'offset') && ~isempty(scaling.offset)
            fs = obj.slave{4};
            offset = arrayfun(@(x) {{strcat('Offset',num2str(x)),
                                  scaling.offset(min(end,x))}}, ...
                            channels);
        end

        rv.PortConfig.input = arrayfun( ...
            @(i) struct('pdo',horzcat(repmat([i-1,1],os_fac,1),...
                                      (0:os_fac-1)',...
                                      repmat(0,os_fac,1)),...
                        'pdo_data_type',sint(16),...
                        'full_scale',fs,...
                        'gain',gain(i), ...
                        'offset',offset(i), ...
                        'portname', strcat('Ch.',num2str(i))), ...
            channels);
        
        if stno_port
            rv.SlaveConfig.sm{end+1} = {2,1,el47x2.pdos{3}};
        
            rv.PortConfig.output(1).portname = 'Start Time Next Output';
            rv.PortConfig.output(1).pdo = [size(rv.SlaveConfig.sm, 2) - 1, 0, 0, 0];
            rv.PortConfig.output(1).pdo_data_type = uint(32);
        end
        
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods (Static)
    %====================================================================
    function test(p)
        ei = EtherCATInfo(fullfile(p,'Beckhoff EL47xx.xml'));
        for i = 1:size(el47x2.models,1)
            fprintf('Testing %s\n', el47x2.models{i,1});
            slave = ei.getSlave(el47x2.models{i,2},...
                    'revision', el47x2.models{i,3});
            model = el47x2.models{i,1};

            for j = 1:14
                rv = el47x2(model).configure(j&1,j,...
                EtherCATSlave.configureScale(2^15,'4'), j&1);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);
            end
        end
    end
end     % methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
properties (Constant)
    %  name          product code         basic_version
    models = {...
      'EL4712', hex2dec('12683052'), hex2dec('00010000'), 2^15;
      'EL4732', hex2dec('127C3052'), hex2dec('00030000'), 2^15;
    };
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
properties (Access = private, Constant)
    % ADC and status PDO
    pdos = {{ {hex2dec('1680'), [hex2dec('7800'),  1, 16]},  % Ch.1 Cycle Count
              {hex2dec('1600'), [hex2dec('7000'),  1, 16]}}, % Ch.1 Value
            { {hex2dec('1780'), [hex2dec('7800'),  2, 16]},  % Ch.2 Cycle Count
              {hex2dec('1700'), [hex2dec('7000'),  2, 16]}}, % Ch.2 Value
            { {hex2dec('1a82'), [hex2dec('1d09'), hex2dec('98'), 32]}}, % StartTimeNextOutput
    };

    os_idx_inc = 16;

    % Distributed Clock
    dc = [hex2dec('730'),0,  -1,0,0,1,0,-1,0,0;
          hex2dec('730'),0,  -2,0,0,1,0,-1,0,0;
          hex2dec('730'),0,  -3,0,0,1,0,-1,0,0;
          hex2dec('730'),0,  -4,0,0,1,0,-1,0,0;
          hex2dec('730'),0,  -5,0,0,1,0,-1,0,0;
          hex2dec('730'),0,  -8,0,0,1,0,-1,0,0;
          hex2dec('730'),0, -10,0,0,1,0,-1,0,0;
          hex2dec('730'),0, -16,0,0,1,0,-1,0,0;
          hex2dec('730'),0, -20,0,0,1,0,-1,0,0;
          hex2dec('730'),0, -25,0,0,1,0,-1,0,0;
          hex2dec('730'),0, -32,0,0,1,0,-1,0,0;
          hex2dec('730'),0, -40,0,0,1,0,-1,0,0;
          hex2dec('730'),0, -50,0,0,1,0,-1,0,0;
          hex2dec('730'),0,-100,0,0,1,0,-1,0,0];

end     % properties

end     % classdef
