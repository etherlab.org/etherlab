classdef baumer_eax580 < EtherCATSlave

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        %====================================================================
        function obj = baumer_eax580(id)
            if nargin > 0
                obj.slave = obj.find(id);
            end
        end

        %====================================================================
        function rv = configure(obj,mapped_pdo,sdo_config,dc_config)

            rv.SlaveConfig.vendor = hex2dec('EC');
            rv.SlaveConfig.description = obj.slave{1};
            rv.SlaveConfig.product  = obj.slave{2};

            % Get a list of pdo's for the selected slave
            selected = ismember(1:size(obj.pdo,1), [obj.slave{4},obj.slave{5}]) ...
                & ismember([obj.pdo{:,1}], mapped_pdo);

            % Reduce this list to the ones selected, making sure that
            % excluded pdo's are not mapped
            exclude = [];
            for i = 1:numel(selected)
                selected(i) = selected(i) & ~ismember(baumer_eax580.pdo{i,1}, exclude);
                if selected(i)
                    exclude = [exclude,baumer_eax580.pdo{i,2}];
                end
            end

            % Configure SM2 and SM3
            selected_idx = find(selected);
            rx = obj.slave{4}(ismember(obj.slave{4}, selected_idx));
            tx = obj.slave{5}(ismember(obj.slave{5}, selected_idx));
            rv.SlaveConfig.sm = ...
                {{2,0, arrayfun(@(x) {baumer_eax580.pdo{x,1}, baumer_eax580.pdo{x,3}},...
                                rx, 'UniformOutput', false)}, ...
                 {3,1, arrayfun(@(x) {baumer_eax580.pdo{x,1}, baumer_eax580.pdo{x,3}},...
                                tx, 'UniformOutput', false)}};

            rv.PortConfig.input = struct('portname',{});

            % Configure output port. The algorithm below will group all boolean
            % signals to one port. All other entries get a separate port
            outputs = arrayfun(@(i) arrayfun(@(j) {i-1, ...
                                                   baumer_eax580.pdo{tx(i),3}(baumer_eax580.pdo{tx(i),4}{j,1}(1)+1,3), ...
                                                   baumer_eax580.pdo{tx(i),4}{j,1}',...
                                                   baumer_eax580.pdo{tx(i),4}{j,2},...
                                                   baumer_eax580.pdo{tx(i),4}{j,3}},...
                                            1:size(baumer_eax580.pdo{tx(i),4},1),...
                                            'UniformOutput', false), ...
                              1:numel(tx), 'UniformOutput', false);
            if ~isempty(outputs)
                outputs = horzcat(outputs{:});
            end

            rv.PortConfig.output = ...
                cellfun(@(i) struct('pdo',horzcat(repmat([1,i{1}], ...
                                                         size(i{3})), ...
                                                  i{3}, zeros(size(i{3}))), ...
                                    'pdo_data_type', i{5}, ...
                                    'portname', i{4}), ...
                         outputs);
            if isempty(rv.PortConfig.output)
                rv.PortConfig.output = struct('portname',{});
            end

            % Distributed clocks
            if dc_config(1) == 4
                rv.PortConfig.dc = dc_config(2:11);
            elseif dc_config(1) > 1
                dc = baumer_eax580.dc;
                rv.PortConfig.dc = dc(dc_config(1),:);
                rv.PortConfig.dc(1) = obj.slave{7}; % Set AssignActivate
            end

            % CoE Configuration
            sdo_row = unique([obj.slave{6}, baumer_eax580.pdo{[tx,rx],5}]);
            % Use resolution-dependent default values by ignoring specific
            % sdos which are zero
            sdo_row = setdiff(sdo_row, [2*(sdo_config(2)==0), 11*(sdo_config(11) == 0), 12*(sdo_config(12) == 0)]);
            rv.SlaveConfig.sdo = num2cell(...
                horzcat(obj.sdo(sdo_row,:), sdo_config(sdo_row)'));
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Static)
        %====================================================================
        function modelChanged
            obj = baumer_eax580(get_param(gcbh,'model'));

            obj.updateRevision();

            EtherCATSlave.updatePDOVisibility([obj.pdo{[obj.slave{[4,5]}],1}]);
            baumer_eax580.updatePDO()
        end

        %====================================================================
        function updatePDO
            obj = baumer_eax580(get_param(gcbh,'model'));
            names = char(get_param(gcbh,'MaskNames'));
            values = get_param(gcbh,'MaskValues');

            % row list of all pdo's for the slave
            % Columns:
            %   1: PDO
            %   2: Excluded PDO's
            %   3: Enabled additional SDO's
            pdo_list = baumer_eax580.pdo([obj.slave{[4,5]}], [1,2,5]);

            pdo_rows = ismember(names, ...
                          strcat('pdo_x', dec2hex([pdo_list{:,1}],4)),...
                          'rows');
            pdo_num = zeros(size(pdo_rows));
            pdo_num(pdo_rows) = hex2dec(names(pdo_rows,6:end));
            on = strcmp(values,'on') & pdo_rows;

            selected_rows = ismember([pdo_list{:,1}], pdo_num(on));
            exclude = unique([pdo_list{selected_rows, 2}]);

            disable = pdo_rows & ismember(pdo_num, exclude);

            for i = 1:length(pdo_list)
                if disable(i) && strcmp(values(i), 'on')
                    % Upps, there is a confict here. Uncheck the option
                    % in question and retry the update
                    set_param(gcbh,deblank(names(i,:)),'off')
                    baumer_eax580.updatePDO()
                    return
                end
            end

            EtherCATSlave.setEnable(pdo_rows, ~disable);

            % Append additional SDO's to the list defined in slave{6}
            sdo = unique([obj.slave{6}, pdo_list{selected_rows, 3}]);

            EtherCATSlave.updateSDOVisibility(dec2base(sdo,10,2));
        end

        %====================================================================
        function test(p)
            ei = EtherCATInfo(fullfile(p,'Baumer_EAx580_EtherCAT_Encoders_ESI_V102.xml'));
            for i = 1:size(baumer_eax580.models,1)
                fprintf('Testing %s\n', baumer_eax580.models{i,1});
                slave = ei.getSlave(baumer_eax580.models{i,2},...
                        'revision', baumer_eax580.models{i,3});
                model = baumer_eax580.models{i,1};

                l = [baumer_eax580.models{1,4},baumer_eax580.models{1,5}];

                pdoIdx = cell2mat(baumer_eax580.pdo(l,1))';
                rv = baumer_eax580(model).configure(pdoIdx, 1:50,2);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);

                pdoIdx = cellfun(@(x) x(1), baumer_eax580.pdo(l,2))';
                rv = baumer_eax580(model).configure(pdoIdx, 1:50,2);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);
            end
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (Constant, Access = private)

        pdo = {
                hex2dec('1a00'), hex2dec({'1a01', '1a02', '1a03', '1a04', '1a05', '1a06'}),...
                [ hex2dec('6004'), 0,32], ...
                { 0, 'Position', uint(32) },...
                [];
                hex2dec('1a01'), hex2dec({'1a00', '1a02', '1a03', '1a04', '1a05', '1a06'}),...
                [ hex2dec('6004'), 0,32;
                  hex2dec('2004'), 0,32], ...
                { 0, 'Position', uint(32), 1, 'Speed', sint(32) },...
                [];
                hex2dec('1a02'), hex2dec({'1a00', '1a01', '1a03', '1a04', '1a05', '1a06'}),...
                [ hex2dec('6004'), 0,32;
                  hex2dec('2004'), 0,32;
                  hex2dec('2000'), 0,32;
                  hex2dec('6503'), 0,16;
                  hex2dec('6505'), 0,16], ...
                { 0, 'Position', uint(32);  1, 'Speed', sint(32); ...
                  2, 'System time', uint(32); 3, 'Alarms', uint(16); ...
                  4, 'Warnings', uint(16) },...
                [];
                hex2dec('1a03'), hex2dec({'1a00', '1a01', '1a02', '1a04', '1a05', '1a06'}),...
                [ hex2dec('600C'), 0,32], ...
                { 0, 'Raw Position', uint(32) },...
                [];
                hex2dec('1a04'), hex2dec({'1a00', '1a01', '1a02', '1a03', '1a05', '1a06'}),...
                [ hex2dec('6004'), 0,32;
                  hex2dec('2004'), 0,32;
                  hex2dec('2000'), 0,32;
                  hex2dec('6503'), 0,16;
                  hex2dec('6505'), 0,16;
                  hex2dec('2120'), 0,32], ...
                { 0, 'Position', uint(32);  1, 'Speed', sint(32); ...
                  2, 'System time', uint(32); 3, 'Alarms', uint(16); ...
                  4, 'Warnings', uint(16); 5, 'Temperature', sint(32) },...
                [];
                hex2dec('1a05'), hex2dec({'1a00', '1a01', '1a02', '1a03', '1a04', '1a06'}),...
                [ hex2dec('6004'), 0,32;
                  hex2dec('6505'), 0,16;
                  hex2dec('2000'), 0,32], ...
                { 0, 'Position', uint(32);  1, 'Warnings', uint(16); ...
                  2, 'System time', uint(32) },...
                [];
                hex2dec('1a06'), hex2dec({'1a00', '1a01', '1a02', '1a03', '1a04', '1a05'}),...
                [ hex2dec('2003'), 0,16], ...
                { 0, 'Position (2 bytes)', uint(16) },...
                [];

        }


        dc = [0,0,0,0,0,0,0,0,0,0;      % FreeRun
              0,0,1,0,0,0,0,1,0,0;      % DC-Synchron
              0,0,1,0,0,1,0,1,0,0];     % DC-Synchron (input based)

        sdo = [ hex2dec('2001'),  1, 16; %  1 Gear factor config
                hex2dec('2001'),  2, 32; %  2 Gear numerator (res. dependent default)
                hex2dec('2001'),  3, 32; %  3 Gear denominator
                hex2dec('2002'),  1,  8; %  4 Speed Calc
                hex2dec('2002'),  2,  8; %  5 Speed Unit
                hex2dec('2002'),  3,  8; %  6 Speed Update time
                hex2dec('2002'),  4,  8; %  7 Speed Filter depth
                hex2dec('2005'),  0, 32; %  8 Button preset Value
                hex2dec('2201'),  0, 32; %  9 Expected Cycle Time
                hex2dec('6000'),  0, 16; % 10 Operating parameters
                hex2dec('6001'),  0, 32; % 11 Measuring Units per Revolution (res. dependent default)
                hex2dec('6002'),  0, 32; % 12 Total measuring range (res. dependent default)
        ]
    end

    properties (Constant)
        %      Model,                   ProductCode, RevNo
        %         Rx,              Tx,                   CoE, AssignActivate
        models = {...
            'EAL580',           hex2dec('00000200'), hex2dec('00000001'), ...
                 [],             [1:7],                [1:12], hex2dec('320');
            'EAM580',           hex2dec('00000300'), hex2dec('00000001'), ...
                 [],             [1:7],                [1:12], hex2dec('320');
        }
    end
end
