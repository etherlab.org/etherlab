classdef el60xx < EtherCATSlave

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        %====================================================================
        function obj = el60xx(id)
            if nargin > 0
                obj.slave = obj.find(id);
            end
        end

        function rv = configure(obj,channels, sdo_config)

            rv.SlaveConfig.vendor = 2;
            rv.SlaveConfig.description = obj.slave{1};
            rv.SlaveConfig.product  = obj.slave{2};

            if ~ismember(2, obj.slave{4})
                channels(2) = 0;
            end
            sdo_indices = obj.slave{5};
            pdos = el60xx.pdo(obj.slave{6});


            rx_select = find(channels);
            tx_select = rx_select + floor(size(pdos, 2) / 2);
            rv.SlaveConfig.sm = {...
                 {2,0, arrayfun(@(x) {pdos{x}{1}, pdos{x}{2}},...
                                 rx_select, 'UniformOutput', false)},...
                 {3,1, arrayfun(@(x) {pdos{x}{1}, pdos{x}{2}},...
                                 tx_select, 'UniformOutput', false)}
            };


            inputs = arrayfun(@(i) {...
                arrayfun(@(o) ...
                    struct('pdo', [zeros(size(pdos{rx_select(i)}{3}{o, 1}, 1), 1), ...
                                   repmat(i-1, size(pdos{rx_select(i)}{3}{o, 1}, 1), 1), ...
                                   pdos{rx_select(i)}{3}{o, 1}], ...
                           'pdo_data_type',  pdos{rx_select(i)}{3}{o, 3}, ...
                           'portname',    pdos{rx_select(i)}{3}{o, 2} ...
                           ) ...
                    , 1:size(pdos{rx_select(i)}{3}, 1)) ...
                }, 1:numel(rx_select));

            outputs = arrayfun(@(i) {...
                arrayfun(@(o) ...
                    struct('pdo', [ones(size(pdos{tx_select(i)}{3}{o, 1}, 1), 1), ...
                                   repmat(i-1, size(pdos{tx_select(i)}{3}{o, 1}, 1), 1), ...
                                   pdos{tx_select(i)}{3}{o, 1}], ...
                           'pdo_data_type',  pdos{tx_select(i)}{3}{o, 3}, ...
                           'portname',    pdos{tx_select(i)}{3}{o, 2} ...
                           ) ...
                    , 1:size(pdos{tx_select(i)}{3}, 1)) ...
                }, 1:numel(tx_select));

            if ~isempty(inputs) && ~isempty(outputs)
                rv.PortConfig.input = horzcat(inputs{:});
                rv.PortConfig.output = horzcat(outputs{:});
                sdo_def = el60xx.sdo;
                rv.SlaveConfig.sdo = num2cell(...
                    horzcat(sdo_def(sdo_indices, :), sdo_config(sdo_indices)'));
            else
                rv.PortConfig.input = [];
                rv.PortConfig.output = [];
            end
        end
    end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Static)
        %====================================================================
        function modelChanged()
            obj = el60xx(get_param(gcbh,'model'));

            EtherCATSlave.updateSDOVisibility(dec2base(obj.slave{5},10,2));
            if length(obj.slave{4}) > 1
                EtherCATSlave.setEnable('ch1_enable', 1);
                EtherCATSlave.setEnable('ch2_enable', 1);
            else
                EtherCATSlave.setEnable('ch1_enable', 0);
                EtherCATSlave.setEnable('ch2_enable', 0);
                names = get_param(gcbh,'MaskNames');
                values = get_param(gcbh,'MaskValues');
                values{strcmp(names, 'ch1_enable')} = 'on';
                set_param(gcbh, 'MaskValues', values);
            end

            obj.updateRevision();

        end

        function test(p)
            ei = EtherCATInfo(fullfile(p,'Beckhoff EL6xxx.xml'));
            models = el60xx.models(strncmp(el60xx.models(:,1), 'EL', 2), :);
            el60xx.testModels(ei, models);
        end

        %====================================================================
        function testModels(ei, models)
            for i = 1:size(models,1)
                fprintf('Testing %s\n', models{i,1});
                slave = ei.getSlave(models{i,2},...
                        'revision', models{i,3});

                default_sdo = zeros(1, 22);

                rv = el60xx(models{i, 1}).configure([1; 0], default_sdo);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);

                if numel(models{i, 4}) > 1
                    rv = el60xx(models{i, 1}).configure([1; 1], default_sdo);
                    slave.testConfig(rv.SlaveConfig,rv.PortConfig);
                    rv = el60xx(models{i, 1}).configure([0; 1], default_sdo);
                    slave.testConfig(rv.SlaveConfig,rv.PortConfig);
                end

            end
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (Constant, Access = private)

        %              PdoEntry       EntryIdx, SubIdx, Bitlen
        pdo = {{hex2dec('1604'),[hex2dec('7001'),  1, 16;
                                 hex2dec('7000'), 17,  8;
                                 hex2dec('7000'), 18,  8;
                                 hex2dec('7000'), 19,  8;
                                 hex2dec('7000'), 20,  8;
                                 hex2dec('7000'), 21,  8;
                                 hex2dec('7000'), 22,  8;
                                 hex2dec('7000'), 23,  8;
                                 hex2dec('7000'), 24,  8;
                                 hex2dec('7000'), 25,  8;
                                 hex2dec('7000'), 26,  8;
                                 hex2dec('7000'), 27,  8;
                                 hex2dec('7000'), 28,  8;
                                 hex2dec('7000'), 29,  8;
                                 hex2dec('7000'), 30,  8;
                                 hex2dec('7000'), 31,  8;
                                 hex2dec('7000'), 32,  8;
                                 hex2dec('7000'), 33,  8;
                                 hex2dec('7000'), 34,  8;
                                 hex2dec('7000'), 35,  8;
                                 hex2dec('7000'), 36,  8;
                                 hex2dec('7000'), 37,  8;
                                 hex2dec('7000'), 38,  8;], ...
                                { [0, 0], 'Ch.1 Control', uint(8);
                                  [0, 1], 'Ch.1 Num Bytes', uint(8);
                                  [[1:22]', zeros(22,1)], 'Ch.1 Data', uint(8); };}, ...
            {hex2dec('1605'),   [hex2dec('7011'),  1, 16;
                                 hex2dec('7010'), 17,  8;
                                 hex2dec('7010'), 18,  8;
                                 hex2dec('7010'), 19,  8;
                                 hex2dec('7010'), 20,  8;
                                 hex2dec('7010'), 21,  8;
                                 hex2dec('7010'), 22,  8;
                                 hex2dec('7010'), 23,  8;
                                 hex2dec('7010'), 24,  8;
                                 hex2dec('7010'), 25,  8;
                                 hex2dec('7010'), 26,  8;
                                 hex2dec('7010'), 27,  8;
                                 hex2dec('7010'), 28,  8;
                                 hex2dec('7010'), 29,  8;
                                 hex2dec('7010'), 30,  8;
                                 hex2dec('7010'), 31,  8;
                                 hex2dec('7010'), 32,  8;
                                 hex2dec('7010'), 33,  8;
                                 hex2dec('7010'), 34,  8;
                                 hex2dec('7010'), 35,  8;
                                 hex2dec('7010'), 36,  8;
                                 hex2dec('7010'), 37,  8;
                                 hex2dec('7010'), 38,  8;], ...
                                { [0, 0], 'Ch.2 Control', uint(8);
                                  [0, 1] 'Ch.2 Num Bytes', uint(8);
                                  [[1:22]', zeros(22,1)], 'Ch.2 Data', uint(8); }}, ...
            {hex2dec('1a04'),   [hex2dec('6001'),  1, 16;
                                 hex2dec('6000'), 17,  8;
                                 hex2dec('6000'), 18,  8;
                                 hex2dec('6000'), 19,  8;
                                 hex2dec('6000'), 20,  8;
                                 hex2dec('6000'), 21,  8;
                                 hex2dec('6000'), 22,  8;
                                 hex2dec('6000'), 23,  8;
                                 hex2dec('6000'), 24,  8;
                                 hex2dec('6000'), 25,  8;
                                 hex2dec('6000'), 26,  8;
                                 hex2dec('6000'), 27,  8;
                                 hex2dec('6000'), 28,  8;
                                 hex2dec('6000'), 29,  8;
                                 hex2dec('6000'), 30,  8;
                                 hex2dec('6000'), 31,  8;
                                 hex2dec('6000'), 32,  8;
                                 hex2dec('6000'), 33,  8;
                                 hex2dec('6000'), 34,  8;
                                 hex2dec('6000'), 35,  8;
                                 hex2dec('6000'), 36,  8;
                                 hex2dec('6000'), 37,  8;
                                 hex2dec('6000'), 38,  8;], ...
                                { [0, 0], 'Ch.1 Status', uint(8);
                                  [0, 1] 'Ch.1 Num Bytes', uint(8);
                                  [[1:22]', zeros(22,1)], 'Ch.1 Data', uint(8); }}, ...
            {hex2dec('1a05'),   [hex2dec('6011'),  1, 16;
                                 hex2dec('6010'), 17,  8;
                                 hex2dec('6010'), 18,  8;
                                 hex2dec('6010'), 19,  8;
                                 hex2dec('6010'), 20,  8;
                                 hex2dec('6010'), 21,  8;
                                 hex2dec('6010'), 22,  8;
                                 hex2dec('6010'), 23,  8;
                                 hex2dec('6010'), 24,  8;
                                 hex2dec('6010'), 25,  8;
                                 hex2dec('6010'), 26,  8;
                                 hex2dec('6010'), 27,  8;
                                 hex2dec('6010'), 28,  8;
                                 hex2dec('6010'), 29,  8;
                                 hex2dec('6010'), 30,  8;
                                 hex2dec('6010'), 31,  8;
                                 hex2dec('6010'), 32,  8;
                                 hex2dec('6010'), 33,  8;
                                 hex2dec('6010'), 34,  8;
                                 hex2dec('6010'), 35,  8;
                                 hex2dec('6010'), 36,  8;
                                 hex2dec('6010'), 37,  8;
                                 hex2dec('6010'), 38,  8;], ...
                                { [0, 0], 'Ch.2 Status', uint(8);
                                  [0, 1] 'Ch.2 Num Bytes', uint(8);
                                  [[1:22]', zeros(22,1)], 'Ch.2 Data', uint(8); }}, ...
            {hex2dec('1602'),[   hex2dec('3003'),  1, 16;
                                 hex2dec('3003'),  2,  8;
                                 hex2dec('3003'),  3,  8;
                                 hex2dec('3003'),  4,  8;
                                 hex2dec('3003'),  5,  8;
                                 hex2dec('3003'),  6,  8;
                                 hex2dec('3003'),  7,  8;
                                 hex2dec('3003'),  8,  8;
                                 hex2dec('3003'),  9,  8;
                                 hex2dec('3003'), 10,  8;
                                 hex2dec('3003'), 11,  8;
                                 hex2dec('3003'), 12,  8;
                                 hex2dec('3003'), 13,  8;
                                 hex2dec('3003'), 14,  8;
                                 hex2dec('3003'), 15,  8;
                                 hex2dec('3003'), 16,  8;
                                 hex2dec('3003'), 17,  8;
                                 hex2dec('3003'), 18,  8;
                                 hex2dec('3003'), 19,  8;
                                 hex2dec('3003'), 20,  8;
                                 hex2dec('3003'), 21,  8;
                                 hex2dec('3003'), 22,  8;
                                 hex2dec('3003'), 23,  8;], ...
                                { [0, 0], 'Control', uint(8);
                                  [0, 1], 'Num Bytes', uint(8);
                                  [[1:22]', zeros(22,1)], 'Data', uint(8); };}, ...
            {hex2dec('1a02'),   [hex2dec('3103'),  1, 16;
                                 hex2dec('3103'),  2,  8;
                                 hex2dec('3103'),  3,  8;
                                 hex2dec('3103'),  4,  8;
                                 hex2dec('3103'),  5,  8;
                                 hex2dec('3103'),  6,  8;
                                 hex2dec('3103'),  7,  8;
                                 hex2dec('3103'),  8,  8;
                                 hex2dec('3103'),  9,  8;
                                 hex2dec('3103'), 10,  8;
                                 hex2dec('3103'), 11,  8;
                                 hex2dec('3103'), 12,  8;
                                 hex2dec('3103'), 13,  8;
                                 hex2dec('3103'), 14,  8;
                                 hex2dec('3103'), 15,  8;
                                 hex2dec('3103'), 16,  8;
                                 hex2dec('3103'), 17,  8;
                                 hex2dec('3103'), 18,  8;
                                 hex2dec('3103'), 19,  8;
                                 hex2dec('3103'), 20,  8;
                                 hex2dec('3103'), 21,  8;
                                 hex2dec('3103'), 22,  8;
                                 hex2dec('3103'), 23,  8;], ...
                                { [0, 0], 'Status', uint(8);
                                  [0, 1] 'Num Bytes', uint(8);
                                  [[1:22]', zeros(22,1)], 'Data', uint(8); }}, ...
        };

        sdo = [
            hex2dec('8000'),  1,  8;    %  1 RTS/CTS (EL6002)
            hex2dec('8000'),  2,  8;    %  2 XON/XOFF Tx
            hex2dec('8000'),  3,  8;    %  3 XON/XOFF RX
            hex2dec('8000'),  4,  8;    %  4 FIFO Continuous
            hex2dec('8000'),  5,  8;    %  5 Transfer rate optimization
            hex2dec('8000'),  6,  8;    %  6 Half Duplex (EL6022)
            hex2dec('8000'),  7,  8;    %  7 RS422 P2P (EL6022)
            hex2dec('8000'), 17,  8;    %  8 Baud Rate
            hex2dec('8000'), 21,  8;    %  9 Data Frame (8N1)
            hex2dec('8000'), 26, 16;    % 10 Rx Buffer Full Notification
            hex2dec('8000'), 27, 32;    % 11 Explicit Baudrate (EL6002 FW 03)

            hex2dec('8010'),  1,  8;    % 12 RTS/CTS (EL6002)
            hex2dec('8010'),  2,  8;    % 13 XON/XOFF Tx
            hex2dec('8010'),  3,  8;    % 14 XON/XOFF RX
            hex2dec('8010'),  4,  8;    % 15 FIFO Continuous
            hex2dec('8010'),  5,  8;    % 16 Transfer rate optimization
            hex2dec('8010'),  6,  8;    % 17 Half Duplex (EL6022)
            hex2dec('8010'),  7,  8;    % 18 RS422 P2P (EL6022)
            hex2dec('8010'), 17,  8;    % 19 Baud Rate
            hex2dec('8010'), 21,  8;    % 20 Data Frame (8N1)
            hex2dec('8010'), 26, 16;    % 21 Rx Buffer Full Notification
            hex2dec('8010'), 27, 32;    % 22 Explicit Baudrate (EL6002 FW 03)
        ];

    end

    properties (Constant)
        %   Model           ProductCode         RevisionNo
        %    Channels       SDO                 PDO
        models = {...
            'EL6001',           hex2dec('17713052'), hex2dec('00150000'), ...
            [1],      [2:5, 8:10],         [5;6];
            'EL6002',           hex2dec('17723052'), hex2dec('00130000'), ...
            [1;2],    [1:5, 8:10, 12:16, 19:21],  (1:4)';
            'EL6021',           hex2dec('17853052'), hex2dec('00160000'), ...
            [1], [2:10], [5;6];
            'EL6022',           hex2dec('17863052'), hex2dec('00130000'), ...
            [1;2], [2:10, 13:21], (1:4)';
            'EL6001-0000-0016', hex2dec('17713052'), hex2dec('00100000'), ...
            [1],      [2:5, 8:10],         [5;6];
            'EL6001-0000-0017', hex2dec('17713052'), hex2dec('00110000'), ...
            [1],      [2:5, 8:10],         [5;6];
            'EL6001-0000-0018', hex2dec('17713052'), hex2dec('00120000'), ...
            [1],      [2:5, 8:10],         [5;6];
            'EL6001-0000-0019', hex2dec('17713052'), hex2dec('00130000'), ...
            [1],      [2:5, 8:10],         [5;6];
            'EL6001-0000-0020', hex2dec('17713052'), hex2dec('00140000'), ...
            [1],      [2:5, 8:10],         [5;6];
            'EL6002-0000-0016', hex2dec('17723052'), hex2dec('00100000'), ...
            [1;2],    [1:5, 8:10, 12:16, 19:21],  (1:4)';
            'EL6002-0000-0017', hex2dec('17723052'), hex2dec('00110000'), ...
            [1;2],    [1:5, 8:10, 12:16, 19:21],  (1:4)';
            'EL6021-0000-0016', hex2dec('17853052'), hex2dec('00100000'), ...
            [1], [2:10], [5;6];
            'EL6021-0000-0017', hex2dec('17853052'), hex2dec('00110000'), ...
            [1], [2:10], [5;6];
            'EL6021-0000-0018', hex2dec('17853052'), hex2dec('00120000'), ...
            [1], [2:10], [5;6];
            'EL6021-0000-0019', hex2dec('17853052'), hex2dec('00130000'), ...
            [1], [2:10], [5;6];
            'EL6021-0000-0020', hex2dec('17853052'), hex2dec('00140000'), ...
            [1], [2:10], [5;6];
            'EL6021-0000-0021', hex2dec('17853052'), hex2dec('00150000'), ...
            [1], [2:10], [5;6];
            'EL6002-0000-0018', hex2dec('17723052'), hex2dec('00120000'), ...
            [1;2],    [1:5, 8:10, 12:16, 19:21],  (1:4)';
            'EL6022-0000-0016', hex2dec('17863052'), hex2dec('00100000'), ...
            [1;2], [2:10, 13:21], (1:4)';
            'EL6022-0000-0017', hex2dec('17863052'), hex2dec('00110000'), ...
            [1;2], [2:10, 13:21], (1:4)';
            'EL6022-0000-0018', hex2dec('17863052'), hex2dec('00120000'), ...
            [1;2], [2:10, 13:21], (1:4)';
        };

    end
end
