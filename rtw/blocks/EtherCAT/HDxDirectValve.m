classdef HDxDirectValve < EtherCATSlave
    methods
        function obj = HDxDirectValve(id)
            if nargin > 0
                obj.slave = obj.find(id);
            end
        end

        function rv = configure(obj,firmware_flavour,analogInputs)
            rv.SlaveConfig.vendor = 36;
            rv.SlaveConfig.product = obj.slave{2};
            rv.SlaveConfig.description = strcat('Rexroth CiA408\\n', obj.slave{1});
            rv.PortConfig.input = [];
            rv.PortConfig.output = [];

            is_hdb = strcmp(firmware_flavour, 'HDB (IAC)');

            configuredAnalogInputIdx = find(analogInputs > 1);
            numAnalogInputs = nnz(configuredAnalogInputIdx);
            if numAnalogInputs > 3
                errordlg('Only up to three Analog inputs supported', gcb);
                return
            end

            rv.SlaveConfig.sdo = HDxDirectValve.sdo([2-is_hdb, 3:size(HDxDirectValve.sdo,1)],:);

            inputIsCurrent = analogInputs > 6;
            inputIsVoltage = 1 < analogInputs & analogInputs < 6;
            if is_hdb
                if nnz(inputIsCurrent(1:2)) && nnz(inputIsVoltage(1:2))
                    errordlg('Both Analog In 1 and 2 must be either Current or Voltate', gcb)
                    return
                end
                rv.SlaveConfig.sdo(1, 4) = 1 * logical(nnz(inputIsCurrent(1:2))) + 2 * inputIsCurrent(3) + 8 * inputIsCurrent(4);
            else
                rv.SlaveConfig.sdo(1, 4) =  [1, 2, 4, 8] * inputIsCurrent;
            end

            txpdo_select = [1;2; configuredAnalogInputIdx + 2];
            % max 4 pdo entries per pdo, see Rexroth RD 30330-FK
            numTxPdos = ceil(numel(txpdo_select) / 4);
            txpdo_select_matrix = arrayfun(@(i) ...
                    txpdo_select(1 + 4*(i-1):min(4*i, numel(txpdo_select))), 1:numTxPdos, 'UniformOutput', false);

            rv.SlaveConfig.sm = { ...
                {2, 0, {
                    {hex2dec('1600'),...
                    cell2mat(cellfun(@(x) x{1}, HDxDirectValve.rx_pdo, 'UniformOutput', false)')
                    }
                 }}, ...
                {3, 1, ...
                    arrayfun(@(pdo) { hex2dec('1a00') + pdo - 1, ...
                        cell2mat(cellfun(@(x) x{1},...
                        HDxDirectValve.tx_pdo(cell2mat(txpdo_select_matrix(:, pdo))),...
                        'UniformOutput', false)')
                    }, 1:numTxPdos, 'UniformOutput', false)
                }
            };

            rv.PortConfig.input = cell2mat(arrayfun(@(i) struct(...
                    'pdo', [...
                        zeros(size(HDxDirectValve.rx_pdo{i}{2}{1}, 1), 2), ...
                        repmat(i-1, size(HDxDirectValve.rx_pdo{i}{2}{1})), ...
                        HDxDirectValve.rx_pdo{i}{2}{1}],...
                    'pdo_data_type', HDxDirectValve.rx_pdo{i}{2}{2}, ...
                    'portname', HDxDirectValve.rx_pdo{i}{2}{3}, ...
                    'full_scale', HDxDirectValve.rx_pdo{i}{2}{4}...
                    )...
                , 1:size(HDxDirectValve.rx_pdo, 2), 'UniformOutput', false)');

            rv.PortConfig.output = cell2mat(arrayfun(@(pdo) ...
                    cell2mat(arrayfun(@(i) struct(...
                        'pdo', [...
                            ones(size(HDxDirectValve.tx_pdo{txpdo_select_matrix{pdo}(i)}{2}{1})), ...
                            repmat(pdo-1, size(HDxDirectValve.tx_pdo{txpdo_select_matrix{pdo}(i)}{2}{1})), ...
                            repmat(i-1, size(HDxDirectValve.tx_pdo{txpdo_select_matrix{pdo}(i)}{2}{1})), ...
                            HDxDirectValve.tx_pdo{txpdo_select_matrix{pdo}(i)}{2}{1}],...
                        'pdo_data_type', HDxDirectValve.tx_pdo{txpdo_select_matrix{pdo}(i)}{2}{2}, ...
                        'portname', HDxDirectValve.tx_pdo{txpdo_select_matrix{pdo}(i)}{2}{3}, ...
                        'full_scale', HDxDirectValve.tx_pdo{txpdo_select_matrix{pdo}(i)}{2}{4}...
                        )...
                    , 1:size(txpdo_select_matrix{pdo}, 1), 'UniformOutput', false)'), ...
                    1:numTxPdos, 'UniformOutput', false)');

            % P-0-2900.i.1
            rv.SlaveConfig.sdo((1:numAnalogInputs) + 4, 4) = ...
                arrayfun(@(i) HDxDirectValve.AnalogInRanges(analogInputs(i), 1) + i, ...
                         configuredAnalogInputIdx);
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Static)
        %====================================================================
        function modelChanged
            obj = HDxDirectValve(get_param(gcbh,'model'));
            names = char(get_param(gcbh,'MaskNames'));
            values = get_param(gcbh,'MaskValues');
            keys = arrayfun(@(i) sprintf('analogInRange%d', i), 1:4, 'UniformOutput', false);

            anaInIdx = ismember(names, keys);

            hasWireBreak = ismember(values(anaInIdx), HDxDirectValve.HasWirebreak);

            keys = arrayfun(@(i) sprintf('analogInWireBreak%d', i), 1:4, 'UniformOutput', false);

            EtherCATSlave.setEnable(keys, hasWireBreak)

        end

    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (Constant, Access = private)

        rx_pdo = {
            {...
                [hex2dec('3dc4'), hex2dec('01'),  32], ... % CiA 408 Control word P-0-3524
                {(0:3)', uint(1), 'Disable, Hold, Active, Reset', []}
            },...
            {...
                [hex2dec('235c'), hex2dec('01'),  32], ... % Target spool position S-0-0860
                {     0, sint(32), 'Target spool position [-1.0, 1.0]', 100 * 1000 }
            },...
        };

        tx_pdo = {
            {...
                [hex2dec('3dc5'), hex2dec('01'),  32], ... % CiA 408 Statusword P-0-3525
                {(0:3)', uint(1), 'Disable, Hold, Active, Ready', []}
            },...
            {...
                [hex2dec('235e'), hex2dec('01'),  32], ... % Current spool position S-0-0862
                {     0, sint(32), 'Current spool position [-2.0, 1.0]', 100 * 1000 }
            },...
            {
                [hex2dec('30d2'), hex2dec('01'),  16], ... % P-0-0210 Analog in 1
                {     0, sint(16), 'Analog Input 1', 1000}
            }, ...
            {
                [hex2dec('30d3'), hex2dec('01'),  16], ... % P-0-0211 Analog in 2
                {     0, sint(16), 'Analog Input 2', 1000}
            }, ...
            {
                [hex2dec('30e4'), hex2dec('01'),  16], ... % P-0-0228 Analog in 3
                {     0, sint(16), 'Analog Input 3', 1000}
            }, ...
            {
                [hex2dec('30e5'), hex2dec('01'),  16], ... % P-0-0229 Analog in 4
                {     0, sint(16), 'Analog Input 4', 1000}
            }, ...
        };


        sdo = [
            hex2dec('3dc2'), 1, 32,              0; %  1 P-0-2918.0.1 Hardware Analog Input Output (HDB only)
            hex2dec('3ea1'), 1, 32,              0; %  1 P-0-2918.0.4 Hardware Analog Input Output (HDE only)
            hex2dec('210d'), 1, 16,              0; %  2 S-0-0269: Disable permanent sdo storage to not destroy the flash
            hex2dec('3ff4'), 1, 16, hex2dec('113'); %  3 P-0-4084: Fluid Power Profile
            hex2dec('2020'), 1, 16,  hex2dec('51'); %  4 S-0-0032: Direct Valve Control
            hex2dec('3d9a'), 1, 16,              0; %  5 P-0-2900.2.1 Analog in Assignment 1
            hex2dec('3d9b'), 1, 16,              0; %  6 P-0-2900.3.1 Analog in Assignment 2
            hex2dec('3d9c'), 1, 16,              0; %  7 P-0-2900.4.1 Analog in Assignment 3
        ];

        AnalogInRanges = [
            % Keep in sync with popups in Mask
            % Value for ctrl
            0; % Disabled
            hex2dec('000'); % 0..10V
            hex2dec('100'); % +-10V
            hex2dec('200'); % 1.0..10V
            hex2dec('300'); % 0.5..5V
            hex2dec('400'); % 0.1..10V
            hex2dec('800'); % 0..20mA
            hex2dec('900'); % 4..20mA
        ];
    end
    properties (Constant)
        % Model                ProductCode    Revision
        models = {...
            'HDx20V12+ CoE',   hex2dec('00246801'),  3;
            'HDx20V10 CoE',    hex2dec('00246801'),  2;
            'HDx17/18/19 CoE', hex2dec('00246801'),  1;
        };
    end
end
