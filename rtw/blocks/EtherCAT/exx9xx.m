classdef exx9xx < EtherCATSlave

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        %====================================================================
        function obj = exx9xx(id)
            if nargin > 0
                obj.slave = obj.find(id);
            end
        end

        %====================================================================
        function rv = configure(obj, mapinputs, mapoutputs, vector)
            rv.SlaveConfig.vendor = 2;
            rv.SlaveConfig.description = obj.slave{1};
            rv.SlaveConfig.product  = obj.slave{2};

            rv.SlaveConfig.sm = {
                {2, 0, arrayfun(@(i) exx9xx.pdo{i}(1,[1, 2]), obj.slave{4}, 'UniformOutput', false)}, ...
                {3, 1, arrayfun(@(i) exx9xx.pdo{i}(1,[1, 2]), obj.slave{5}, 'UniformOutput', false)}, ...
            };
            if obj.slave{6} > 0
                % map sync manager 6 pdos
                rv.SlaveConfig.sm{end + 1} = ...
                    {6, 0, {exx9xx.pdo{obj.slave{6}}(1,[1, 2])}};
            end

            rv.PortConfig.input(1).pdo = [0,0];
            rv.PortConfig.input(1).portname = 'FSoE';
            rv.PortConfig.input(1).pdo_data_type = [];

            rv.PortConfig.output(1).pdo = [1,0];
            rv.PortConfig.output(1).portname = 'FSoE';
            rv.PortConfig.output(1).pdo_data_type = [];

            if mapinputs && size(exx9xx.pdo{obj.slave{5}}, 2) > 2
                tmp = exx9xx.pdo(obj.slave{5});
                if vector
                    tmp{1}{3}.pdo_data_type = uint(1);
                    rv.PortConfig.output(end + 1) = tmp{1}{3};
                else
                    for o = tmp{1}{4}
                        o.pdo_data_type = uint(1);
                        rv.PortConfig.output(end + 1) = o;
                    end
                end
            end

            for i = obj.slave{4}
                if mapoutputs && size(exx9xx.pdo{i}, 2) > 2 && obj.slave{6} == 0
                    tmp = exx9xx.pdo(i);
                    if vector
                        tmp{1}{3}.pdo_data_type = uint(1);
                        rv.PortConfig.input(end + 1) = tmp{1}{3};
                    else
                        for o = tmp{1}{4}
                            o.pdo_data_type = uint(1);
                            rv.PortConfig.input(end + 1) = o;
                        end
                    end
                end
            end
            if mapoutputs && obj.slave{6} > 0 && size(exx9xx.pdo{obj.slave{6}}, 2) > 2
                tmp = exx9xx.pdo(obj.slave{6});
                if vector
                    tmp{1}{3}.pdo_data_type = uint(1);
                    rv.PortConfig.input(end + 1) = tmp{1}{3};
                else
                    for o = tmp{1}{4}
                        o.pdo_data_type = uint(1);
                        rv.PortConfig.input(end + 1) = o;
                    end
                end
            end
        end

    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Static)
        %====================================================================
        function test(p)
            % use patched esi file for el1918,
            % copy TxPdo and RxPdo entries from slots to devices.
            ei = EtherCATInfo(fullfile(p,'Beckhoff ELx9xx.xml'));
            models = exx9xx.models(strncmp(exx9xx.models(:,1), 'EL1904', 6) | ...
                                   strncmp(exx9xx.models(:,1), 'EP1908', 6) | ...
                                   strncmp(exx9xx.models(:,1), 'EL2904', 6), :);
            exx9xx.testModels(ei, models);
            ei = EtherCATInfo(fullfile(p,'Beckhoff EL19xx.xml'));
            models = exx9xx.models(strncmp(exx9xx.models(:,1), 'EL1918', 6), :);
            exx9xx.testModels(ei, models);
            ei = EtherCATInfo(fullfile(p,'Beckhoff EL29xx.xml'));
            models = exx9xx.models(strncmp(exx9xx.models(:,1), 'EL291', 5), :);
            exx9xx.testModels(ei, models);
            ei = EtherCATInfo(fullfile(p,'Beckhoff EPx9xx.xml'));
            models = exx9xx.models(strncmp(exx9xx.models(:,1), 'EP1918', 6) | ...
                                   strncmp(exx9xx.models(:,1), 'EP2918', 6) | ...
                                   strncmp(exx9xx.models(:,1), 'EP1957', 6), :);
            exx9xx.testModels(ei, models);
        end

        %====================================================================
        function testModels(ei, models)
            for i = 1:size(models,1)
                fprintf('Testing %s\n', models{i,1});
                slave = ei.getSlave(models{i,2},...
                        'revision', models{i,3});


                rv = exx9xx(models{i,1}).configure(1, 0, 0);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);

                rv = exx9xx(models{i,1}).configure(1, 1, 0);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);

                rv = exx9xx(models{i,1}).configure(0, 1, 0);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);

                rv = exx9xx(models{i,1}).configure(0, 0, 0);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);

                rv = exx9xx(models{i,1}).configure(1, 0, 1);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);

                rv = exx9xx(models{i,1}).configure(1, 1, 1);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);

                rv = exx9xx(models{i,1}).configure(0, 1, 1);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);

                rv = exx9xx(models{i,1}).configure(0, 0, 1);
                slave.testConfig(rv.SlaveConfig,rv.PortConfig);
            end
        end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (Constant)
        pdo = {
            % pdo, [entry_idx, entry_subidx, size], port Vector,
            % port separated,
            {hex2dec('1600'), [ %  1
                    hex2dec('7000'), hex2dec('01'),   8; % Command
                                  0,             0,   8; % padding
                    hex2dec('7000'), hex2dec('02'),  16; % CRC
                    hex2dec('7000'), hex2dec('03'),  16; % Connection ID
            ]},
            {hex2dec('1a00'), [ %  2
                    hex2dec('6000'), hex2dec('01'),   8; % Command
                    hex2dec('6001'), hex2dec('01'),   1; % Input
                    hex2dec('6001'), hex2dec('02'),   1; %
                    hex2dec('6001'), hex2dec('03'),   1; %
                    hex2dec('6001'), hex2dec('04'),   1; %
                                  0,             0,   4; % padding
                    hex2dec('6000'), hex2dec('03'),  16; % CRC
                    hex2dec('6000'), hex2dec('04'),  16; % Connection ID
            ], struct('pdo', [repmat([1,0],4,1), (1:4)', zeros(4,1)], ...
                      'portname', 'Safe In') , ...
               arrayfun(@(i) struct('pdo', [1, 0, i, 0], ...
                                      'portname',sprintf('Safe In %d', i)), ...
                               1:4) },
            {hex2dec('1600'), [ %  3
                    hex2dec('7080'), hex2dec('01'),   8; % Command
                    hex2dec('7002'), hex2dec('01'),   1; % Err Ack
                    hex2dec('7012'), hex2dec('01'),   1;
                    hex2dec('7022'), hex2dec('01'),   1;
                    hex2dec('7032'), hex2dec('01'),   1;
                    hex2dec('7042'), hex2dec('01'),   1;
                    hex2dec('7052'), hex2dec('01'),   1;
                    hex2dec('7062'), hex2dec('01'),   1;
                    hex2dec('7072'), hex2dec('01'),   1;
                    hex2dec('7080'), hex2dec('03'),  16; % CRC
                    hex2dec('7080'), hex2dec('02'),  16; % Connection ID
            ]},
            {hex2dec('1a00'), [ %  4
                    hex2dec('6080'), hex2dec('01'),   8; % Command
                    hex2dec('6001'), hex2dec('01'),   1; % Input
                    hex2dec('6002'), hex2dec('01'),   1; % Fault
                    hex2dec('6011'), hex2dec('01'),   1; % Input
                    hex2dec('6012'), hex2dec('01'),   1; % Fault
                    hex2dec('6021'), hex2dec('01'),   1; % Input
                    hex2dec('6022'), hex2dec('01'),   1; % Fault
                    hex2dec('6031'), hex2dec('01'),   1; % Input
                    hex2dec('6032'), hex2dec('01'),   1; % Fault
                    hex2dec('6041'), hex2dec('01'),   1; % Input
                    hex2dec('6042'), hex2dec('01'),   1; % Fault
                    hex2dec('6051'), hex2dec('01'),   1; % Input
                    hex2dec('6052'), hex2dec('01'),   1; % Fault
                    hex2dec('6061'), hex2dec('01'),   1; % Input
                    hex2dec('6062'), hex2dec('01'),   1; % Fault
                    hex2dec('6071'), hex2dec('01'),   1; % Input
                    hex2dec('6072'), hex2dec('01'),   1; % Fault
                    hex2dec('6080'), hex2dec('03'),  16; % CRC
                    hex2dec('6080'), hex2dec('02'),  16; % Connection ID
            ], struct('pdo', [repmat([1,0],8,1), 1 + 2*(0:7)', zeros(8,1)], ...
                      'portname', 'Safe In'), ...
               arrayfun(@(i) struct('pdo', [1, 0, 2*i-1, 0], ...
                                      'portname',sprintf('Safe In %d', i)), ...
                               1:8)},
            {hex2dec('1600'), [ %  5
                    hex2dec('7000'), hex2dec('01'),   8; % Command
                                  0,             0,   7; % padding
                    hex2dec('7001'), hex2dec('01'),   1; % Err Ack
                    hex2dec('7000'), hex2dec('03'),  16; % CRC
                    hex2dec('7000'), hex2dec('02'),  16; % Connection ID
            ]},
            {hex2dec('1a00'), [ %  6
                    hex2dec('6000'), hex2dec('01'),   8; % Command
                    hex2dec('6001'), hex2dec('01'),   1; % Input
                    hex2dec('6001'), hex2dec('02'),   1; %
                    hex2dec('6001'), hex2dec('03'),   1; %
                    hex2dec('6001'), hex2dec('04'),   1; %
                    hex2dec('6001'), hex2dec('05'),   1; %
                    hex2dec('6001'), hex2dec('06'),   1; %
                    hex2dec('6001'), hex2dec('07'),   1; %
                    hex2dec('6001'), hex2dec('08'),   1; %
                    hex2dec('6000'), hex2dec('03'),  16; % CRC
                    hex2dec('6000'), hex2dec('02'),  16; % Connection ID
            ], struct('pdo', [repmat([1,0],8,1), (1:8)', zeros(8,1)], ...
                      'portname', 'Safe In') , ...
               arrayfun(@(i) struct('pdo', [1, 0, i, 0], ...
                                      'portname',sprintf('Safe In %d', i)), ...
                               1:8) },
            {hex2dec('1600'), [ %  7
                    hex2dec('7040'), hex2dec('01'),   8; % Command
                    hex2dec('7002'), hex2dec('01'),   1; % Err Ack
                    hex2dec('7012'), hex2dec('01'),   1;
                    hex2dec('7022'), hex2dec('01'),   1;
                    hex2dec('7032'), hex2dec('01'),   1;
                    0              , 0            ,   4; % padding
                    hex2dec('7040'), hex2dec('03'),  16; % CRC
                    hex2dec('7040'), hex2dec('02'),  16; % Connection ID
            ]},
            {hex2dec('1a00'), [ %  8
                    hex2dec('6040'), hex2dec('01'),   8; % Command
                    hex2dec('6001'), hex2dec('01'),   1; % Input 1
                    hex2dec('6001'), hex2dec('02'),   1; % Input 2
                    hex2dec('6002'), hex2dec('01'),   1; % Fault
                    hex2dec('6011'), hex2dec('01'),   1; % Input 3
                    hex2dec('6011'), hex2dec('02'),   1; % Input 4
                    hex2dec('6012'), hex2dec('01'),   1; % Fault
                    hex2dec('6021'), hex2dec('01'),   1; % Input 5
                    hex2dec('6021'), hex2dec('02'),   1; % Input 6
                    hex2dec('6022'), hex2dec('01'),   1; % Fault
                    hex2dec('6031'), hex2dec('01'),   1; % Input 7
                    hex2dec('6031'), hex2dec('02'),   1; % Input 8
                    hex2dec('6032'), hex2dec('01'),   1; % Fault
                    0              , 0            ,   4; % padding
                    hex2dec('6040'), hex2dec('03'),  16; % CRC
                    hex2dec('6040'), hex2dec('02'),  16; % Connection ID
            ], struct('pdo', [repmat([1,0],8,1), [1; 2; 4; 5; 7; 8; 10; 11], zeros(8,1)], ...
                      'portname', 'Safe In'), ...
               cellfun(@(i) struct('pdo', [1, 0, i{2}, 0], ...
                                      'portname',sprintf('Safe In %d', i{1})), ...
                {{1, 1}, {2, 2}, {3, 4}, {4, 5}, {5, 7}, {6, 8}, {7, 10}, {8, 11}})},
            {hex2dec('1600'), [ %  9
                    hex2dec('7080'), hex2dec('01'),   8; % Command
                    hex2dec('7001'), hex2dec('01'),   1; % Output
                    hex2dec('7002'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7011'), hex2dec('01'),   1; % Output
                    hex2dec('7012'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7021'), hex2dec('01'),   1; % Output
                    hex2dec('7022'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7031'), hex2dec('01'),   1; % Output
                    hex2dec('7032'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7041'), hex2dec('01'),   1; % Output
                    hex2dec('7042'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7051'), hex2dec('01'),   1; % Output
                    hex2dec('7052'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7061'), hex2dec('01'),   1; % Output
                    hex2dec('7062'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7071'), hex2dec('01'),   1; % Output
                    hex2dec('7072'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7080'), hex2dec('03'),  16; % CRC
                    hex2dec('7080'), hex2dec('02'),  16; % Connection ID
            ]},
            {hex2dec('1a00'), [ % 10
                    hex2dec('6080'), hex2dec('01'),   8; % Command
                    hex2dec('6002'), hex2dec('01'),   1; % Fault
                    hex2dec('6012'), hex2dec('01'),   1; % Fault
                    hex2dec('6022'), hex2dec('01'),   1; % Fault
                    hex2dec('6032'), hex2dec('01'),   1; % Fault
                    hex2dec('6042'), hex2dec('01'),   1; % Fault
                    hex2dec('6052'), hex2dec('01'),   1; % Fault
                    hex2dec('6062'), hex2dec('01'),   1; % Fault
                    hex2dec('6072'), hex2dec('01'),   1; % Fault
                    hex2dec('6080'), hex2dec('03'),  16; % CRC
                    hex2dec('6080'), hex2dec('02'),  16; % Connection ID
            ]},
            {hex2dec('17fe'), [ % 11
                    hex2dec('7003'), hex2dec('01'),   1; % Ch.1 Standard Output
                    hex2dec('7013'), hex2dec('01'),   1; % Ch.2 Standard Output
                    hex2dec('7023'), hex2dec('01'),   1; % Ch.3 Standard Output
                    hex2dec('7033'), hex2dec('01'),   1; % Ch.4 Standard Output
                    hex2dec('7043'), hex2dec('01'),   1; % Ch.5 Standard Output
                    hex2dec('7053'), hex2dec('01'),   1; % Ch.6 Standard Output
                    hex2dec('7063'), hex2dec('01'),   1; % Ch.7 Standard Output
                    hex2dec('7073'), hex2dec('01'),   1; % Ch.8 Standard Output
            ],  struct('pdo', [repmat([2,0],8,1), (0:7)', zeros(8,1)], ...
                       'portname', 'Std. Out'), ...
                arrayfun(@(i) struct('pdo', [2, 0, i - 1, 0], ...
                                     'portname',sprintf('Std. Out %d', i)), ...
                         1:8)},
            {hex2dec('1600'), [ % 12
                    hex2dec('7020'), hex2dec('01'),   8; % Command
                    hex2dec('7001'), hex2dec('01'),   1; % Output
                    hex2dec('7002'), hex2dec('01'),   1; % Output ErrAck
                    hex2dec('7012'), hex2dec('01'),   1; % Input ErrAck
                    0              , 0            ,   5; % padding
                    hex2dec('7020'), hex2dec('03'),  16; % CRC
                    hex2dec('7020'), hex2dec('02'),  16; % Connection ID
            ]},
            {hex2dec('1a00'), [ % 13
                    hex2dec('6020'), hex2dec('01'),   8; % Command
                    hex2dec('6002'), hex2dec('01'),   1; % FSOUT Module Fault
                    hex2dec('6011'), hex2dec('01'),   1; % Input 1
                    hex2dec('6011'), hex2dec('02'),   1; % Input 2
                    hex2dec('6011'), hex2dec('03'),   1; % Input 3
                    hex2dec('6011'), hex2dec('04'),   1; % Input 4
                    hex2dec('6012'), hex2dec('01'),   1; % FSIN Module Fault
                    0              , 0            ,   2; % padding
                    hex2dec('6020'), hex2dec('03'),  16; % CRC
                    hex2dec('6020'), hex2dec('02'),  16; % Connection ID
            ], struct('pdo', [repmat([1,0],4,1), (2:5)', zeros(4,1)], ...
                      'portname', 'Safe In') , ...
               arrayfun(@(i) struct('pdo', [1, 0, i+1, 0], ...
                                      'portname',sprintf('Safe In %d', i)), ...
                               1:4) },
            {hex2dec('17fe'), [ % 14
                    hex2dec('7003'), hex2dec('01'),   1; % Standard out
                    0              , 0            ,   7; % padding
            ], struct('pdo', [2, 0, 0, 0], 'portname', 'Std. Out'), ...
               [struct('pdo', [2, 0, 0, 0], 'portname', 'Std. Out 1'),]},
            {hex2dec('1600'), [ % 15
                    hex2dec('7020'), hex2dec('01'),   8; % Command
                    hex2dec('7001'), hex2dec('01'),   1; % Output 1
                    hex2dec('7002'), hex2dec('01'),   1; % Output 1 ErrAck
                    hex2dec('7011'), hex2dec('01'),   1; % Output 2
                    hex2dec('7012'), hex2dec('01'),   1; % Output 2 ErrAck
                    0              , 0            ,   4; % padding
                    hex2dec('7020'), hex2dec('03'),  16; % CRC
                    hex2dec('7020'), hex2dec('02'),  16; % Connection ID
            ]},
            {hex2dec('1a00'), [ % 16
                    hex2dec('6020'), hex2dec('01'),   8; % Command
                    hex2dec('6002'), hex2dec('01'),   1; % FSOUT Module Fault
                    hex2dec('6012'), hex2dec('01'),   1; % FSOUT Module Fault
                    0              , 0            ,   6; % padding
                    hex2dec('6020'), hex2dec('03'),  16; % CRC
                    hex2dec('6020'), hex2dec('02'),  16; % Connection ID
            ]},
            {hex2dec('17fe'), [ % 17
                    hex2dec('7003'), hex2dec('01'),   1; % Standard out 1
                    hex2dec('7013'), hex2dec('01'),   1; % Standard out 2
                    0              , 0            ,   6; % padding
            ], struct('pdo', [repmat([2,0],2,1), (0:1)', zeros(2,1)], ...
                      'portname', 'Std. Out'), ...
               arrayfun(@(i) struct('pdo', [2, 0, i-1, 0], ...
                                      'portname',sprintf('Std. Out %d', i)), ...
                               1:2) },
            {hex2dec('1600'), [ % 18
                    hex2dec('7000'), hex2dec('01'),   8; % Command
                    hex2dec('7001'), hex2dec('01'),   1; % Output
                    hex2dec('7001'), hex2dec('02'),   1; %
                    hex2dec('7001'), hex2dec('03'),   1; %
                    hex2dec('7001'), hex2dec('04'),   1; %
                    0              , 0            ,   4; % padding
                    hex2dec('7000'), hex2dec('02'),  16; % CRC
                    hex2dec('7000'), hex2dec('03'),  16; % Connection ID
            ]},
            {hex2dec('1a00'), [ % 19
                    hex2dec('6000'), hex2dec('01'),   8; % Command
                                  0,             0,   8; % padding
                    hex2dec('6000'), hex2dec('03'),  16; % CRC
                    hex2dec('6000'), hex2dec('04'),  16; % Connection ID
            ]},
            {hex2dec('1601'), [ % 20
                    hex2dec('7010'), hex2dec('01'),   1; % Std Output 1
                    hex2dec('7010'), hex2dec('02'),   1; % Std Output 2
                    hex2dec('7010'), hex2dec('03'),   1; % Std Output 3
                    hex2dec('7010'), hex2dec('04'),   1; % Std Output 4
                                  0,             0,  12; % padding
            ], struct('pdo', [repmat([0,1],4,1), (0:3)', zeros(4,1)], ...
                      'portname', 'Std. Out'), ...
               arrayfun(@(i) struct('pdo', [0, 1, i-1, 0], ...
                                      'portname',sprintf('Std. Out %d', i)), ...
                               1:4) },
            {hex2dec('1600'), [ % 21
                    hex2dec('7080'), hex2dec('01'),   8; % Command
                    hex2dec('7001'), hex2dec('01'),   1; % Output
                    hex2dec('7002'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7011'), hex2dec('01'),   1; % Output
                    hex2dec('7012'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7021'), hex2dec('01'),   1; % Output
                    hex2dec('7022'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7031'), hex2dec('01'),   1; % Output
                    hex2dec('7032'), hex2dec('01'),   1; % ErrAck
                    hex2dec('7042'), hex2dec('01'),   1; % FSIN 1 ErrAck
                    hex2dec('7052'), hex2dec('01'),   1; % FSIN 2 ErrAck
                    hex2dec('7062'), hex2dec('01'),   1; % FSIN 3 ErrAck
                    hex2dec('7072'), hex2dec('01'),   1; % FSIN 4 ErrAck
                                  0,             0,   4; % padding
                    hex2dec('7080'), hex2dec('03'),  16; % CRC
                    hex2dec('7080'), hex2dec('02'),  16; % Connection ID
            ]},
            {hex2dec('1a00'), [ % 22
                    hex2dec('6080'), hex2dec('01'),   8; % Command
                    hex2dec('6002'), hex2dec('01'),   1; % FSOUT 1 Fault
                    hex2dec('6012'), hex2dec('01'),   1; %
                    hex2dec('6022'), hex2dec('01'),   1; %
                    hex2dec('6032'), hex2dec('01'),   1; % FSOUT 4 Fault
                    hex2dec('6041'), hex2dec('01'),   1; % Input 1
                    hex2dec('6041'), hex2dec('02'),   1; % Input 2
                    hex2dec('6042'), hex2dec('01'),   1; % Fault 1/2
                    hex2dec('6051'), hex2dec('01'),   1; % Input 3
                    hex2dec('6051'), hex2dec('02'),   1; % Input 4
                    hex2dec('6052'), hex2dec('01'),   1; % Fault 3/4
                    hex2dec('6061'), hex2dec('01'),   1; % Input 5
                    hex2dec('6061'), hex2dec('02'),   1; % Input 6
                    hex2dec('6062'), hex2dec('01'),   1; % Fault 5/6
                    hex2dec('6071'), hex2dec('01'),   1; % Input 7
                    hex2dec('6071'), hex2dec('02'),   1; % Input 8
                    hex2dec('6072'), hex2dec('01'),   1; % Fault 7/8
                    hex2dec('6080'), hex2dec('03'),  16; % CRC
                    hex2dec('6080'), hex2dec('02'),  16; % Connection ID
            ], struct('pdo', [repmat([1,0],8,1), [5; 6; 8; 9; 11; 12; 14; 15], zeros(8,1)], ...
                      'portname', 'Safe In'), ...
               cellfun(@(i) struct('pdo', [1, 0, i{2}, 0], ...
                                      'portname',sprintf('Safe In %d', i{1})), ...
                {{1, 5}, {2, 6}, {3, 8}, {4, 9}, {5, 11}, {6, 12}, {7, 14}, {8, 15}})},
            {hex2dec('17fe'), [ % 23
                    hex2dec('7003'), hex2dec('01'),   1; % Ch.1 Standard Output
                    hex2dec('7013'), hex2dec('01'),   1; % Ch.2 Standard Output
                    hex2dec('7023'), hex2dec('01'),   1; % Ch.3 Standard Output
                    hex2dec('7033'), hex2dec('01'),   1; % Ch.4 Standard Output
                                  0,             0,   4; % padding
            ],  struct('pdo', [repmat([2,0],4,1), (0:3)', zeros(4,1)], ...
                       'portname', 'Std. Out'), ...
                arrayfun(@(i) struct('pdo', [2, 0, i - 1, 0], ...
                                     'portname',sprintf('Std. Out %d', i)), ...
                         1:4)},
            {hex2dec('1bfe'), [ % 24
                    hex2dec('f6c0'), hex2dec('01'),  1; % Fieldvoltage Underrange
                    hex2dec('f6c0'), hex2dec('02'),  1; % Fieldvoltage Overrange
                                  0,             0,  6; % padding
            ]},
            {hex2dec('1bff'), [ % 25
                    hex2dec('f100'), hex2dec('01'),  8; % Safe Logic State
                    hex2dec('f100'), hex2dec('02'),  8; % Cycle Counter
            ]},
            {hex2dec('17ff'), [ % 26
                                  0,             0, 16; % padding
            ]},
        };

        % Model       ProductCode          revision             sm2 pdo, sm3 pdo, sm6 pdo
        models = {
            'EL1904',           hex2dec('07703052'), hex2dec('00130000'),       1,  2,  0;
            'EL1918',           hex2dec('077e3052'), hex2dec('00110000'),       3,  4,  0;
            'EL2904',           hex2dec('0b583052'), hex2dec('00130000'), [18,20], 19,  0;
            'EL2911',           hex2dec('0b5f3052'), hex2dec('00100000'),      12, 13, 14;
            'EL2912',           hex2dec('0b603052'), hex2dec('00100000'),      15, 16, 17;
            'EP1908-0002',      hex2dec('07744052'), hex2dec('00140002'),       5,  6,  0;
            'EP1918-0002',      hex2dec('077e4052'), hex2dec('00100002'),       7,  8,  0;
            'EP1957-0022',      hex2dec('07a54052'), hex2dec('00100016'),      21, 22, 23;
            'EP2918-0032',      hex2dec('0b664052'), hex2dec('00100020'), [9,26], [10,24,25], 11;
            'EL1904-0000-0016', hex2dec('07703052'), hex2dec('00100000'),       1,  2,  0;
            'EL1904-0000-0017', hex2dec('07703052'), hex2dec('00110000'),       1,  2,  0;
            'EL1904-0000-0018', hex2dec('07703052'), hex2dec('00120000'),       1,  2,  0;
            'EL1918-0000-0016', hex2dec('077e3052'), hex2dec('00100000'),       3,  4,  0;
            'EP1908-0000-0020', hex2dec('07744052'), hex2dec('00140000'),       5,  6,  0;
            'EP1908-0000-0019', hex2dec('07744052'), hex2dec('00130000'),       5,  6,  0;
            'EP1908-0000-0018', hex2dec('07744052'), hex2dec('00120000'),       5,  6,  0;
            'EP1908-0000-0017', hex2dec('07744052'), hex2dec('00110000'),       5,  6,  0;
            'EP1908-0000-0016', hex2dec('07744052'), hex2dec('00100000'),       5,  6,  0;
        };

    end




end
