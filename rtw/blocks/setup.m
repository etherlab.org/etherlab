function setup()

if isempty(strfind(path,pwd))
    disp(['Adding ' pwd ' to $MATLABPATH']);
    addpath(pwd);
end

disp(['Precompiling functions in ' pwd]);
mex world_time.c
mex raise.c
mex rtipc_tx.c
mex rtipc_rx.c
mex event.c
mex findidx.c
mex etl_message.c
mex propagate_width.c

run EtherCAT/setup.m

return
