function etherlab_packngo(model)

if nargin < 1
    model = bdroot;
end

if ~ischar(model)
    disp('Open a model or call etherlab_packngo(''<model>'')')
    return
end

disp(['Bundling ' model])

% Make sure it is loaded in memory
load_system(model);
modelversion = get_param(model, 'ModelVersion');

% Create temporary directory to pack into
[~, tmpname] = fileparts(tempname);
versionname = strcat(model, '-', modelversion);
tmpdir = fullfile(tmpname, versionname);
mkdir(tmpdir);

etherlabdir = fileparts(which('etherlab.tlc'));

pack_n_go(model, tmpdir, etherlabdir);
create_build_script(model, tmpdir, etherlabdir, modelversion);

[dir,name] = fileparts(tmpdir);
zip(versionname, versionname, tmpname);
rmdir(tmpname, 's');

disp(['Created ', versionname, '.zip']);
disp(['Copy this to your target, unpack it, cd into subdirectory and call ''sh build.sh''']);
disp('Make sure a build environment (gcc, make), pdserv-devel (and possibly ethercat-devel) is installed there');
disp('The bundle contains all other depenendies (including EtherLab/Matlab/Simulink Coder sources)');
disp('Note that Matlab/Simulink Coder sources are copyrighted by The MathWorks, Inc.');

return

%%% Use packNGo() to create a zip file containing generated code
function pack_n_go(model, tmpdir, etherlabdir)

bDirInfo = RTW.getBuildDir(model);
load(fullfile(bDirInfo.BuildDirectory, 'buildInfo.mat'), 'buildInfo');
%you can view this file using  buildInfo.getViewer()

addNonBuildFiles(buildInfo, {[model '.mk'], 'rtw_proj.tmw'}, bDirInfo.BuildDirectory);
addNonBuildFiles(buildInfo, fullfile(matlabroot, 'rtw','c','tools','unixtools.mk'));

args = {'fileName', fullfile(tmpdir, model), 'packType', 'hierarchical'};
v = ver;
if any(strcmp({v.Name}, 'Simulink Coder')) && ~verLessThan('SimulinkCoder', '8.4')
    args = {args{:}, 'minimalHeaders', false};
end
packNGo(buildInfo, args);
zip(fullfile(tmpdir, 'etherlab'), etherlabdir);

%%% Create build file to automate building process
function create_build_script(model, tmpdir, etherlabdir, modelversion)

[~, mldir] = fileparts(matlabroot);
[~, etherlabroot] = fileparts(etherlabdir);

v = ver;
repeat = @(c) repmat(c, numel(v), 1);
space = repeat(' ');
vstring = [repeat('# '), char(v.Name), ...
    space, char(v.Version), ...
    space, char(v.Release), ...
    space, char(v.Date), ...
    repeat(char(10))];

bDirInfo = RTW.getBuildDir(model);

buildfile = fullfile(tmpdir, 'build.sh');
f = fopen(buildfile, 'w');
fprintf(f, '#!/bin/sh\n');
fprintf(f, '\n');
fprintf(f, '# %s, version %s\n', model, modelversion);
fprintf(f, '\n');
fprintf(f, 'MODEL=%s\n', model);
fprintf(f, 'VERSION=%s\n', modelversion);
fprintf(f, 'ZIPFILE=%s.zip\n', model);
fprintf(f, 'MATLAB_ROOT=%s\n', mldir);
fprintf(f, 'MODELDIR=%s\n', bDirInfo.RelativeBuildDir);
fprintf(f, 'ISPC=%i\n', ispc);
fprintf(f, '\n');
fprintf(f, '# MATLAB version %s\n', version);
fprintf(f, '%s', vstring');
fprintf(f, '\n');
fprintf(f, 'unzip -uo etherlab.zip\n');
fprintf(f, 'unzip -uo $ZIPFILE\n');
fprintf(f, 'for z in $(unzip -Z -1 $ZIPFILE); do unzip -uo $z; done\n');
fprintf(f, '\n');
fprintf(f, 'if test $ISPC -eq 1; then\n');
fprintf(f, '  MAKEFILE=$MODELDIR/$MODEL.mk\n');
fprintf(f, '\n');
fprintf(f, '  # Convert DOS end-of-line to UNIX\n');
fprintf(f, '  perl -pe ''s/\\r$//'' -i $MAKEFILE\n');
fprintf(f, '\n');
fprintf(f, '  # Remove MS windows drive symbols (C:\\) and backslashes so make doesn''t puke\n');
fprintf(f, '  perl -pe ''s/([A-Z]):\\\\/\\1_\\\\/g; s#\\\\(?!$)#/#g'' -i $MAKEFILE\n');
fprintf(f, '\n');
fprintf(f, '  OLD_ETHERLAB_DIR=$(make -s -f $MAKEFILE MATLAB_ROOT=$MATLAB_ROOT ECHO_VAR=ETHERLAB_DIR echovariable)\n');
fprintf(f, '\n');
fprintf(f, '  perl -pe "s#$OLD_ETHERLAB_DIR#\\\\\\$\\(ETHERLAB_DIR\\)#g" -i $MAKEFILE\n');
fprintf(f, 'fi\n');
fprintf(f, '\n');
fprintf(f, 'make -C $MODELDIR -f $MODEL.mk START_DIR=.. MATLAB_ROOT=../$MATLAB_ROOT ETHERLAB_DIR=../%s clean all "$@"\n', etherlabroot);
fclose(f);

if isunix
    fileattrib(buildfile, '+x');
end
return
